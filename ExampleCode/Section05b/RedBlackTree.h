/**********************************************
* File: RedBlackTree.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#ifndef RED_BLACK_TREE_H
#define RED_BLACK_TREE_H

#include "dsexceptions.h"
#include <iostream> 
using namespace std;

// Red-black tree class
//
// CONSTRUCTION: with negative infinity object also
//               used to signal failed finds
//
// ******************PUBLIC OPERATIONS*********************
// void insert( x )       --> Insert x
// void remove( x )       --> Remove x (unimplemented)
// bool contains( x )     --> Return true if x is present
// T findMin( )  --> Return smallest item
// T findMax( )  --> Return largest item
// bool isEmpty( )        --> Return true if empty; else false
// void makeEmpty( )      --> Remove all items
// void printTree( )      --> Print tree in sorted order
// ******************ERRORS********************************
// Throws UnderflowException as warranted

template <typename T>
class RedBlackTree
{
  public:
    /**
     * Construct the tree.
     * negInf is a value less than or equal to all others.
     */
    /********************************************
    * Function Name  : RedBlackTree
    * Pre-conditions :  const T & negInf 
    * Post-conditions: explicit
    *  
    ********************************************/
    explicit RedBlackTree( const T & negInf )
    {
        nullNode    = new RedBlackNode;
        nullNode->left = nullNode->right = nullNode;
        
        header      = new RedBlackNode{ negInf };
        header->left = header->right = nullNode;
    }

    /********************************************
    * Function Name  : RedBlackTree
    * Pre-conditions :  const RedBlackTree & rhs 
    * Post-conditions: none
    *  
    ********************************************/
    RedBlackTree( const RedBlackTree & rhs )
    {
        nullNode    = new RedBlackNode;
        nullNode->left = nullNode->right = nullNode;
        
        header      = new RedBlackNode{ rhs.header->element };
        header->left = nullNode;
        header->right = clone( rhs.header->right );
    }

    /********************************************
    * Function Name  : RedBlackTree
    * Pre-conditions :  RedBlackTree && rhs 
    * Post-conditions: none
    *  
    ********************************************/
    RedBlackTree( RedBlackTree && rhs )
      : nullNode{ rhs.nullNode }, header{ rhs.header }
    {
        rhs.nullNode = nullptr;
        rhs.header = nullptr;
    }

    /********************************************
    * Function Name  : ~RedBlackTree
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    ~RedBlackTree( )
    {
        makeEmpty( );
        delete nullNode;
        delete header;
    }
    
    /**
     * Deep copy.
     */
    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  const RedBlackTree & rhs 
    * Post-conditions: RedBlackTree &
    *  
    ********************************************/
    RedBlackTree & operator=( const RedBlackTree & rhs )
    {
        RedBlackTree copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
        
    /**
     * Move.
     */
    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  RedBlackTree && rhs 
    * Post-conditions: RedBlackTree &
    *  
    ********************************************/
    RedBlackTree & operator=( RedBlackTree && rhs )
    {
        std::swap( header, rhs.header );
        std::swap( nullNode, rhs.nullNode );
        
        return *this;
    }

    /********************************************
    * Function Name  : findMin
    * Pre-conditions :  
    * Post-conditions: const T &
    *  
    ********************************************/
    const T & findMin( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };

        RedBlackNode *itr = header->right;

        while( itr->left != nullNode )
            itr = itr->left;

        return itr->element;
    }

    /********************************************
    * Function Name  : findMax
    * Pre-conditions :  
    * Post-conditions: const T &
    *  
    ********************************************/
    const T & findMax( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };

        RedBlackNode *itr = header->right;

        while( itr->right != nullNode )
            itr = itr->right;

        return itr->element;
    }

    /********************************************
    * Function Name  : contains
    * Pre-conditions :  const T & x 
    * Post-conditions: bool
    *  
    ********************************************/
    bool contains( const T & x ) const
    {
        nullNode->element = x;
        RedBlackNode *curr = header->right;

        for( ; ; )
        {
            if( x < curr->element )
                curr = curr->left;
            else if( curr->element < x )
                curr = curr->right;
            else
                return curr != nullNode;
        }
    }

    /********************************************
    * Function Name  : isEmpty
    * Pre-conditions :  
    * Post-conditions: bool
    *  
    ********************************************/
    bool isEmpty( ) const
    {
        return header->right == nullNode;
    }

    /********************************************
    * Function Name  : printTree
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    void printTree( ) const
    {
        if( header->right == nullNode )
            cout << "Empty tree" << endl;
        else
            printTree( header->right );
    }

    /********************************************
    * Function Name  : makeEmpty
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    void makeEmpty( )
    {
        if( header == nullptr )
            return;
        
        reclaimMemory( header->right );
        header->right = nullNode;
    }

    /**
     * Insert item x into the tree. Does nothing if x already present.
     */
    /********************************************
    * Function Name  : insert
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    *  
    ********************************************/
    void insert( const T & x )
    {
        current = parent = grand = header;
        nullNode->element = x;

        while( current->element != x )
        {
            great = grand; grand = parent; parent = current;
            current = x < current->element ?  current->left : current->right;

                // Check if two red children; fix if so
            if( current->left->color == RED && current->right->color == RED )
                handleReorient( x );
        }

            // Insertion fails if already present
        if( current != nullNode )
            return;
        current = new RedBlackNode{ x, nullNode, nullNode };

            // Attach to parent
        if( x < parent->element )
            parent->left = current;
        else
            parent->right = current;
        handleReorient( x );
    }

    /********************************************
    * Function Name  : remove
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    *  
    ********************************************/
    void remove( const T & x )
    {
        cout << "Sorry, remove unimplemented; " << x <<
                " still present" << endl;
    }

  private:
    enum { RED, BLACK };
    
    struct RedBlackNode
    {
        T    element;
        RedBlackNode *left;
        RedBlackNode *right;
        int           color;

        /********************************************
        * Function Name  : RedBlackNode
        * Pre-conditions :  const T & theElement = T{ }, RedBlackNode *lt = nullptr, RedBlackNode *rt = nullptr, int c = BLACK 
        * Post-conditions: none
        *  
        ********************************************/
        RedBlackNode( const T & theElement = T{ }, RedBlackNode *lt = nullptr, RedBlackNode *rt = nullptr, int c = BLACK )
          : element{ theElement }, left{ lt }, right{ rt }, color{ c } { }
        

        /********************************************
        * Function Name  : RedBlackNode
        * Pre-conditions :  T && theElement, RedBlackNode *lt = nullptr, RedBlackNode *rt = nullptr, int c = BLACK 
        * Post-conditions: none
        *  
        ********************************************/
        RedBlackNode( T && theElement, RedBlackNode *lt = nullptr, RedBlackNode *rt = nullptr, int c = BLACK )
          : element{ std::move( theElement ) }, left{ lt }, right{ rt }, color{ c } { }
    };

    RedBlackNode *header;   // The tree header (contains negInf)
    RedBlackNode *nullNode;

        // Used in insert routine and its helpers (logically static)
    RedBlackNode *current;
    RedBlackNode *parent;
    RedBlackNode *grand;
    RedBlackNode *great;

        // Usual recursive stuff
    /********************************************
    * Function Name  : reclaimMemory
    * Pre-conditions :  RedBlackNode *t 
    * Post-conditions: none
    *  
    ********************************************/
    void reclaimMemory( RedBlackNode *t )
    {
        if( t != t->left )
        {
            reclaimMemory( t->left );
            reclaimMemory( t->right );
            delete t;
        }
    }

    /********************************************
    * Function Name  : printTree
    * Pre-conditions :  RedBlackNode *t 
    * Post-conditions: none
    *  
    ********************************************/
    void printTree( RedBlackNode *t ) const
    {
        if( t != t->left )
        {
            printTree( t->left );
            cout << t->element << endl;
            printTree( t->right );
        }
    }

    /********************************************
    * Function Name  : clone
    * Pre-conditions :  RedBlackNode * t 
    * Post-conditions: RedBlackNode *
    *  
    ********************************************/
    RedBlackNode * clone( RedBlackNode * t ) const
    {
        if( t == t->left )  // Cannot test against nullNode!!!
            return nullNode;
        else
            return new RedBlackNode{ t->element, clone( t->left ),
                                     clone( t->right ), t->color };
    }

        // Red-black tree manipulations
    /**
     * Internal routine that is called during an insertion if a node has two red
     * children. Performs flip and rotations. item is the item being inserted.
     */
    /********************************************
    * Function Name  : handleReorient
    * Pre-conditions :  const T & item 
    * Post-conditions: none
    *  
    ********************************************/
    void handleReorient( const T & item )
    {
            // Do the color flip
        current->color = RED;
        current->left->color = BLACK;
        current->right->color = BLACK;

        if( parent->color == RED )   // Have to rotate
        {
            grand->color = RED;
            if( item < grand->element != item < parent->element )
                parent = rotate( item, grand );  // Start dbl rotate
            current = rotate( item, great );
            current->color = BLACK;
        }
        header->right->color = BLACK; // Make root black
    }

    /**
     * Internal routine that performs a single or double rotation.
     * Because the result is attached to the parent, there are four cases.
     * Called by handleReorient.
     * item is the item in handleReorient.
     * theParent is the parent of the root of the rotated subtree.
     * Return the root of the rotated subtree.
     */
    /********************************************
    * Function Name  : rotate
    * Pre-conditions :  const T & item, RedBlackNode *theParent 
    * Post-conditions: RedBlackNode *
    *  
    ********************************************/
    RedBlackNode * rotate( const T & item, RedBlackNode *theParent )
    {
        if( item < theParent->element )
        {
            item < theParent->left->element ?
                rotateWithLeftChild( theParent->left )  :  // LL
                rotateWithRightChild( theParent->left ) ;  // LR
            return theParent->left;
        }
        else
        {
            item < theParent->right->element ?
                rotateWithLeftChild( theParent->right ) :  // RL
                rotateWithRightChild( theParent->right );  // RR
            return theParent->right;
        }
    }

    /********************************************
    * Function Name  : rotateWithLeftChild
    * Pre-conditions :  RedBlackNode * & k2 
    * Post-conditions: none
    *  
    ********************************************/
    void rotateWithLeftChild( RedBlackNode * & k2 )
    {
        RedBlackNode *k1 = k2->left;
        k2->left = k1->right;
        k1->right = k2;
        k2 = k1;
    }

    /********************************************
    * Function Name  : rotateWithRightChild
    * Pre-conditions :  RedBlackNode * & k1 
    * Post-conditions: none
    *  
    ********************************************/
    void rotateWithRightChild( RedBlackNode * & k1 )
    {
        RedBlackNode *k2 = k1->right;
        k1->right = k2->left;
        k2->left = k1;
        k1 = k2;
    }
};

#endif
